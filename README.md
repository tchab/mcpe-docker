# Description

A quick'n'dirty Docker container for the [MCPE project](https://github.com/ChristopherHX/mcpelauncher-manifest) 
to play Minecraft Bedrock Edition on Linux.