FROM ubuntu:19.10

ARG DEBIAN_FRONTEND=noninteractive

ARG USER_ID=1000
ARG USERNAME=abc

ENV DEB_ROOT_URL="https://github.com/ChristopherHX/linux-packaging-scripts/releases/download/amd64.ubuntu.eoan.19.10"

# Update and install dependencies
RUN dpkg --add-architecture i386 \
    && apt update \
    && apt -y dist-upgrade \
    && apt -y install --no-install-recommends \
        wget xorg zenity \
        xboxdrv libgl1-mesa-glx libgl1-mesa-dri \
        libegl1-mesa:i386 libegl1-mesa-dev:i386 libasound2:i386 libasound2-plugins:i386 pulseaudio-utils:i386 pulseaudio-module-jack:i386 jackd2:i386 \
    && rm -rf /var/lib/apt/lists/*

# Installing MCPE Launcher
RUN wget -q "${DEB_ROOT_URL}"/msa-daemon-029fde6-Linux.deb \
    && wget -q "${DEB_ROOT_URL}"/msa-ui-qt-029fde6-Linux.deb \
    && wget -q "${DEB_ROOT_URL}"/mcpelauncher-client-45f9d98-Linux.deb \
    && wget -q "${DEB_ROOT_URL}"/mcpelauncher-core-45f9d98-Linux.deb \
    && wget -q "${DEB_ROOT_URL}"/mcpelauncher-ui-qt-8b7543e-Linux.deb \
    && apt update && apt -y --no-install-recommends install ./*.deb \
    && rm -rf /var/lib/apt/lists/* \
    && echo \
'default-server = unix:/run/user/$USER_ID$/pulse/native\n\
autospawn = no\n\
daemon-binary = /bin/true\n\
enable-shm = false\n' | sed 's/\$USER_ID\$/'${USER_ID}'/g' > /etc/pulse/client.conf \
    && useradd -u ${USER_ID} -m ${USERNAME} \
    && usermod -a -G audio,video,input ${USERNAME}

WORKDIR /home/${USERNAME}

USER ${USERNAME}

CMD [ "mcpelauncher-ui-qt" ]
