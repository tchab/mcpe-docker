#!/usr/bin/env sh

APP_NAME="mcpe-docker"

USER_ID="$(id --user)"
GROUP_ID="$(id --group)"

mkdir -p "${HOME}/.${APP_NAME}"

docker run -it --rm \
    --name "${APP_NAME}" \
    -e DISPLAY \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    --device /dev/dri:/dev/dri \
    -v /run/user/"${USER_ID}"/pulse:/run/user/"${USER_ID}"/pulse \
    --device /dev/input/js0:/dev/input/js0 \
    -v "${HOME}"/."${APP_NAME}":/home/abc \
    --user="${USER_ID}:${GROUP_ID}" \
    ${APP_NAME}
